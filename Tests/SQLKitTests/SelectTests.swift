import XCTest
@testable import SQLKit

class SelectTests: XCTestCase {
    func testAllColumnsNoTableName() {
        let select = Select()
        XCTAssertEqual(select.description, "SELECT * FROM")
    }

    func testAllColumns() {
        let select = Select() 
            .from("MyTable")
        
        XCTAssertEqual(select.description, "SELECT * FROM MyTable")
    }
    
    func testSomeColumnsNoTableName() {
        let select = Select("col1", "col2", "col3")
        XCTAssertEqual(select.description, "SELECT col1, col2, col3 FROM")
    }
    
    func testSomeColumns() {
        let select = Select("col1", "col2", "col3")
            .from("MyTable")
        XCTAssertEqual(select.description, "SELECT col1, col2, col3 FROM MyTable")
    }

    static var allTests : [(String, (SelectTests) -> () throws -> Void)] {
        return [
            ("testAllColumnsNoTableName", testAllColumnsNoTableName),
            ("testAllColumns", testAllColumns),
            ("testSomeColumnsNoTableName", testSomeColumnsNoTableName)
        ]
    }
}
