import XCTest
@testable import SQLKitTests

XCTMain([
     testCase(SQLKitTests.allTests),
])
