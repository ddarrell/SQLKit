//
//  Select.swift
//  SQLKit
//
//  Created by Charles Levesque on 2016-09-25.
//
//

import Foundation

/**
 * @class Select
 * @brief Represents a SELECT SQL statement.
 *
 * This class is designed to be fluent. Therefore, calls is encouraged to be
 * in the form of:
 *
 * Select()
 *     .from("mytable")
 * ...
 */
class Select : CustomStringConvertible {
    private var columns: [String]
    private var tableName: String

    var description: String {
        let columnsToQuery = self.columns.isEmpty ? "*" : self.columns.joined(separator: ", ")
        return ("SELECT \(columnsToQuery) " +
            "FROM \(self.tableName) ").trimmingCharacters(in: CharacterSet(charactersIn: " "))
    }
    
    /**
     * @brief Default constructor of a select.
     *
     * This by default select all of the columns ("*").
     */
    init() {
        self.columns = []
        self.tableName = String()
    }
    
    /**
     * @brief Constructor with sepecified columns.
     *
     * Here you can specify columns you want to retreive by passing their names.
     * @param columns The name of the columns to retreive.
     */
    convenience init(_ columns: String...) {
        self.init()
        self.columns.append(contentsOf: columns)
    }
    
    /**
     * @brief Add a table name to the query.
     *
     * @param tableName The name of the table where to do the query
     */
    func from(_ tableName: String) -> Select {
        self.tableName = tableName
        return self
    }
}
